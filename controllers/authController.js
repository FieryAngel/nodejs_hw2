const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  const checkUser = await User.findOne({username});

  if (checkUser) {
    return res.status(400).json({
      message: 'User with username ' +
        `${username} already exists`,
    });
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  res.status(200).json({message: 'New user created successfully'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({
      message: `No user with username ` +
        `${username} found`,
    });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({username: user.username}, JWT_SECRET);

  res.status(200).json({message: 'Success', jwt_token: token});
};
