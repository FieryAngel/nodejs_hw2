const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('../routers/middlewares/authMiddleware');
const {noteValidate} = require('./middlewares/noteExistMiddleware');
const {asyncWrapper} = require('./helpers');

const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

router.get('/', authMiddleware, asyncWrapper(async (req, res) => {
  const {offset, limit} = req.body;

  const user = await User.findOne({username: req.user.username});
  const notes = await Note.find({userId: user._id}, {__v: 0})
      .skip(offset).limit(limit);

  res.status(200).json({notes: notes});
}));

router.post('/', authMiddleware, asyncWrapper(async (req, res) => {
  const user = await User.findOne({username: req.user.username});
  const userId = user._id;

  const note = new Note({
    userId: userId,
    text: req.body.text,
  });

  await note.save();

  res.status(200).json({message: 'New note created successfully'});
}));

router.get('/:id', authMiddleware, noteValidate,
    asyncWrapper(async (req, res) => {
      const note = await Note.findById(req.params.id, {__v: 0});

      res.status(200).json({note: note});
    }));

router.put('/:id', authMiddleware, noteValidate,
    asyncWrapper(async (req, res) => {
      const {text} = req.body;

      if (!text) {
        return res.status(400).json({message: `'text' field is required`});
      }

      await Note.findByIdAndUpdate(req.params.id, {$set: {text: text}});

      res.status(200).json({message: 'Note updated successfully'});
    }));

router.patch('/:id', authMiddleware, noteValidate,
    asyncWrapper(async (req, res) => {
      const value = await Note.findById(req.params.id);

      await Note.findByIdAndUpdate(req.params.id,
          {$set: {completed: !value.completed}});

      res.status(200)
          .json({message: `'completed' field was changed successfully`});
    }));

router.delete('/:id', authMiddleware, noteValidate,
    asyncWrapper(async (req, res) => {
      await Note.findByIdAndDelete(req.params.id);

      res.status(200).json({message: 'Note deleted successfully'});
    }));

module.exports = router;
