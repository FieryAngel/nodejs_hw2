const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();

const {authMiddleware} = require('../routers/middlewares/authMiddleware');
const {userExistValidate} =
  require('../routers/middlewares/userExistMiddleware');
const {asyncWrapper} = require('./helpers');

const {User} = require('../models/userModel');

router.get('/me', authMiddleware, userExistValidate,
    asyncWrapper(async (req, res) => {
      const user = await User.findOne({username: req.user.username},
          {__v: 0, password: 0});

      res.status(200).json({user: user});
    }));

router.delete('/me', authMiddleware, userExistValidate,
    asyncWrapper(async (req, res) => {
      await User.findOneAndDelete({username: req.user.username});

      res.status(200).json({message: 'User deleted successfully'});
    }));

router.patch('/me', authMiddleware, userExistValidate,
    asyncWrapper(async (req, res) => {
      const {oldPassword, newPassword} = req.body;

      const user = await User.findOne({username: req.user.username});

      if (!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({message: `Wrong old password!`});
      }

      await User.findOneAndUpdate({username: user.username},
          {$set: {password: await bcrypt.hash(newPassword, 10)}});

      res.status(200).json({message: 'Password updated successfully'});
    }));

module.exports = router;
