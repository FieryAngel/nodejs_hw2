const {Note} = require('../../models/noteModel');

module.exports.noteValidate = async (req, res, next) => {
  const note = await Note.findById(req.params.id, {__v: 0});

  if (!note) {
    return res.status(400).json({message: 'Note does not exist'});
  }

  next();
};
