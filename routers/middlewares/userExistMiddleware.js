const {User} = require('../../models/userModel');

module.exports.userExistValidate = async (req, res, next) => {
  const user = await User.findOne({username: req.user.username}, {__v: 0});

  if (!user) {
    return res.status(400).json({message: 'User does not exist'});
  }

  next();
};
