const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', noteRouter);

app.use((err, req, res, next) => {
  res.status(400).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://testuser:123TestUser321@cluster0.luc6s.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
